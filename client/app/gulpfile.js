var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')


gulp.task('default'); 
gulp.task('scripts', function () {
 gulp.src([
 		   'bower_components/angular/angular.js',
 		   'bower_components/jquery/dist/jquery.min.js',
 		   'bower_components/angular-route/angular-route.js',
 		   'bower_components/angular-resource/angular-resource.js',
 		   'bower_components/angular-ui-router/release/angular-ui-router.min.js', 
 		   'js/*.js' ])
   .pipe(concat('app.js'))
   .pipe(ngAnnotate())
   .pipe(uglify())
   .pipe(gulp.dest('gulp'))
});
