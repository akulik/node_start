/**
 * Created by kulik on 2/12/15.
 */
var mongoose    = require('mongoose');

mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;

var Schema = mongoose.Schema;

// Schemas
var User = new Schema({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String, required: true},
    modified: { type: Date, default: Date.now }
});

// validation
User.path('email').validate(function (v) {
    return v.length > 5 && v.length < 70;
});

var UserModel = mongoose.model('User', User);

exports.UserModel = UserModel;