//var express = require('express');
var config = require('./libs/config');
var log = require('./libs/log')(module);
var app = require('./expressApi').app;
app.listen(config.get('port'), function () {
    log.info('Express server listening on port ' + config.get('port'));
});