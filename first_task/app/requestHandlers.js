/**
 * Created by drew on 09.02.15.
 */

var querystring = require("querystring"),
    fs = require("fs"),
    Handlebars = require("handlebars"),
    UserModel = require('./libs/mongoose').UserModel;
function start(response, postData, method, id) {
    var post_data = querystring.parse(postData);
    if (method == "POST") {
        fs.exists("./app/view/text_form_result.html", function (exists) {
            if (!exists) {
                fs.createReadStream("./app/view/404.html").pipe(response);
            } else {
                fs.readFile("./app/view/text_form_result.html", 'utf8', function (error, file) {
                    if (error) {
                        fs.createReadStream("./app/view/404.html").pipe(response);
                    } else {
                        var template = Handlebars.compile(file);
                        var data = { "name": post_data.name, "text": post_data.text};
                        var result = template(data);
                        response.writeHead(200, {"Content-Type": "text/html"});
                        response.write(result, "binary");
                        response.end();
                    }
                })

            }
        })
    } else {
        fs.exists("./app/view/text_form.html", function (exists) {
            if (exists) {
                fs.createReadStream("./app/view/text_form.html").pipe(response);
            } else {
                fs.createReadStream("./app/view/404.html").pipe(response);
            }
        })
    }
}

function picture(response, postData, method, id) {
    fs.readFile("./app/view/picture.html", 'utf8', function (error, file) {
        if (error) {
            fs.createReadStream("./app/view/404.html").pipe(response);
        } else {
            var template = Handlebars.compile(file);
            var data = { "css": './public/css/style_picture.css', "SRC": "./public/img/node-hosting.png"};
            var result = template(data);
            response.writeHead(200, {"Content-Type": "text/html"});
            response.write(result, "binary");
            response.end();
        }
    });
}

function users(response, postData, method, id) {
    var post_data = querystring.parse(postData);
    switch (method) {
        case "GET":
            if (id) {
                fs.readFile("./app/view/users/user_form.html", 'utf8', function (error, file) {
                    if (error) {
                        fs.createReadStream("./app/view/404.html").pipe(response);
                    } else {
                        UserModel.find({ _id: id },function (err, user) {
                            var template = Handlebars.compile(file);
                            getUsersTable(response, template, user[0]);
                        })
                    }
                });
            } else {
                fs.readFile("./app/view/users/user_form.html", 'utf8', function (error, file) {
                    if (error) {
                        fs.createReadStream("./app/view/404.html").pipe(response);
                    } else {
                        var template = Handlebars.compile(file);
                        getUsersTable(response, template,false);
                    }
                });
            }
            break;
        case "POST":
            if (postData.length > 0) {

                var user = new UserModel(post_data);
                user.save(function (err) {
                    if (err) return err;
                    getUsersTable(response,false,false);
                });
                break;
            } else {
                fs.createReadStream("./app/view/404.html").pipe(response);
                break;
            }

        case "DELETE":
            UserModel.remove({ _id: post_data.id }, function(){
                getUsersTable(response,false,false);
            });
            break;
        case "PUT":
            console.log(post_data);
            UserModel.findByIdAndUpdate(post_data._id , post_data , function(err) {
                if (err) fs.createReadStream("./app/view/404.html").pipe(response);
                getUsersTable(response,false,false);
            });
            break;
        default:
            fs.createReadStream("./app/view/404.html").pipe(response);
            break
    }
}

function getUsersTable(response, template, user) {

    UserModel = require('./libs/mongoose').UserModel;
    UserModel.find(function (err, users) {
        if (err) return err;
        fs.readFile("./app/view/users/user_table.html", 'utf8', function (error, file) {
            if (error) {
                fs.createReadStream("./app/view/404.html").pipe(response);
            } else {

                var templateTable = Handlebars.compile(file);
                var table = templateTable({"users": users});
                if (template) {
                    var data = {'button_text': "Add user", 'table': table, 'user': user};
                    var result = template(data);
                } else {
                    var result = table;
                }

                response.writeHead(200, {"Content-Type": "text/html"});
                response.write(result, "binary");
                response.end();
            }
        });
    });
}

exports.start = start;
exports.picture = picture;
exports.users = users;