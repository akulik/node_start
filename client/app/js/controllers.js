'use strict';

/* Controllers */

var articlesControllers = angular.module('articlesControllers', []);

    articlesControllers.controller('DashboardCtrl', function($location,$state, ItemsModel,AdditionArticleInfo,UserService){
            var dashboard = this;
            if (!UserService.getCurrentUser()) {
                $location.path('/login');
            }
            $state.go('articles.new'); 

            function getItems() {
                dashboard.items = ItemsModel.query();
                dashboard.items.$promise.then(function (result) {
                        angular.forEach(result,function(value,key){
                        if(value.user_id !== undefined) {
                            AdditionArticleInfo.articleOwner(value.id).then(function (resultOwner) {
                                if (resultOwner.data) value.email = resultOwner.data.email;    
                            });
                        }         
                    });
                });

            }

            function createItem(item) {
                ItemsModel.save(item,function(u, putResponseHeaders) {
                    if (!u.error){
                        dashboard.editedItem = u;
                        dashboard.isEditing = true;
                        getItems();
                    } else {
                        dashboard.errorCreateMessage = u.error.message;
                    }
                });            
            }

            function updateItem(item) {
                if (item.user_id == undefined) item.user_id = UserService.getCurrentUser().userId; 
                ItemsModel.update(item,function(u, putResponseHeaders) {
                    initCreateForm();
                    getItems();
                });
            }

            function deleteItem(itemId) {
                ItemsModel.delete({id:itemId},function(u, putResponseHeaders) {
                    initCreateForm();
                    getItems();
                    cancelEditing();
                    $state.go('articles.new'); 
                });
            }

            function initCreateForm() {
                dashboard.newItem = { title: '', text: '', author: '', description: '',user_id: dashboard.currentUser.userId };
            }

            function setEditedItem(item) {
                dashboard.editedItem = angular.copy(item);
            }

            function isCurrentItem(itemId) {
                return dashboard.editedItem !== null && dashboard.editedItem.id === itemId;
            }

            function cancelEditing() {
                dashboard.editedItem = null;
            }

            function addSubmitClass(e) {
                $(e.target).parent('form').find('input, textarea').addClass('submitClicked');
            }

            dashboard.currentUser = UserService.getCurrentUser();
            dashboard.items = [];
            dashboard.editedItem = null;
            dashboard.isEditing = false;
            dashboard.getItems = getItems;
            dashboard.createItem = createItem;
            dashboard.updateItem = updateItem;
            dashboard.deleteItem = deleteItem;
            dashboard.setEditedItem = setEditedItem;
            dashboard.isCurrentItem = isCurrentItem;
            dashboard.cancelEditing = cancelEditing;
            dashboard.addSubmitClass = addSubmitClass;
            dashboard.errorCreateMessage = false;
            initCreateForm();
            getItems();
        });


    articlesControllers.controller('LoginCtrl', function($rootScope, $location, LoginService, UserService){
        var login = this;
        if (UserService.getCurrentUser()) {
                $location.path('/articles');
            }
        function signIn(user) {
            LoginService.login(user)
                .then(function(response) {
                    if (!response.data.error && response.data) {
                        user.access_token = response.data.id;
                        user.userId = response.data.userId;
                        UserService.setCurrentUser(user);
                        $rootScope.$broadcast('authorized');
                        $location.path('/articles');
                    } else {
                        UserService.setCurrentUser(user);
                        $rootScope.$broadcast('unauthorized');
                    }
                });
        }

        function register(user) {
            LoginService.register(user)
                .then(function(response) {
                    signIn(user);
                });
        }

        function submit(user) {
            login.newUser ? register(user) : signIn(user);
        }

        login.newUser = false;
        login.submit = submit;
    });


    articlesControllers.controller('MainCtrl', function($rootScope,$location,LoginService, UserService) {
        var main = this;
        function logout() {
            LoginService.logout()
                .then(function(response) {
                    main.currentUser = UserService.setCurrentUser(null);
                    $location.path('/login');
                }, function(error) {
                    console.log(error);
                });
        }

        $rootScope.$on('authorized', function() {
            main.errorMessage = null;
            main.currentUser = UserService.getCurrentUser();
        });

        $rootScope.$on('unauthorized', function() {
            main.currentUser  = UserService.setCurrentUser(null);            
            main.errorMessage = "Wrong!!!";
            $location.path('/login');
        });
        main.logout = logout;
        main.currentUser = UserService.getCurrentUser();        
    });