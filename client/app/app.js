'use strict';

/* App Module */

var articlesApp = angular.module('articlesApp', ['ui.router',
  'angular-storage', 
  'articlesControllers',
  'articlesServices'])
  .constant('ENDPOINT_URI', 'http://localhost:1414/api/')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'partials/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
            .state('articles', {
                url: '/articles',
                templateUrl: 'partials/articles.html',
                controller: 'DashboardCtrl',
                controllerAs: 'dashboard'
            })
            .state('articles.edit', {
                url: '/edit',
                templateUrl: 'partials/articles.edit.html',
            })
            .state('articles.new', {
                url: '',
                templateUrl: 'partials/articles.new.html',
            });

        $urlRouterProvider.otherwise('/articles');

        $httpProvider.interceptors.push('APIInterceptor');
    })