var winston = require('winston');
var config      = require('./config');
var MongoDBLog = require('winston-mongodb').MongoDB;

function getLogger(module) {
    var path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports : [
            new winston.transports.Console({
                colorize:   true,
                level:      'debug',
                label:      path
            }),
            new MongoDBLog({
                port:       config.get('port'),
                collection: config.get('collectionLog'),
                dbUri:      config.get('mongoose:uri')
            })
        ]
    });
}

module.exports = getLogger;