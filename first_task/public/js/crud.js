/**
 * Created by kulik on 2/13/15.
 */
$(function(){
    $('form').submit( function(){
        var type = "POST";
        var data = $('form').serialize();

        if ($('#users_crud_submit_button').data('put')) {
            type = "PUT";
        }
        $.ajax({
            type: type,
            url: "/users",
            data: data,
            dataType: "html",
            success: function(result){
                $('table').remove();
                $(".right").append(result);
            }
        });
        return false;
    })

    $("body").on("click",".delete",function(){
        $.ajax({
            type: "DELETE",
            url: "/users",
            data:  {"id":$(this).data("id")},
            dataType: "html",
            success: function(result){
                $('table').remove();
                $(".right").append(result);
            }
        });
        return false
    })
})