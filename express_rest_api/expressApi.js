/**
 * Created by kulik on 2/19/15.
 */
var express = require('express');
var path = require('path');
var log = require('./libs/log')(module);
var ArticleModel = require('./libs/mongoose').ArticleModel;
var CommentsModel = require('./libs/mongoose').CommentsModel;
var app = express();

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));

app.use(function (req, res, next) {
    res.status(404);
    log.debug('Not found URL: %s', req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    log.error('Internal error(%d): %s', res.statusCode, err.message);
    res.send({ error: err.message });
    return;
});

app.get('/api', function (req, res) {
    res.send('API is running');
});

app.get('/api/articles', function (req, res) {
    return ArticleModel.find(function (err, articles) {
        if (!err) {
            return res.send(articles);
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.post('/api/articles', function (req, res) {
    var article = new ArticleModel({
        title: req.body.title,
        author: req.body.author,
        description: req.body.description,
        images: req.body.images
    });

    article.save(function (err) {
        if (!err) {
            log.info("article created");
            return res.send({ status: 'OK', article: article });
        } else {
            if (err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
    });
});

app.get('/api/articles/:id', function (req, res) {
    return ArticleModel.findById(req.params.id, function (err, article) {
        if (!article) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        if (!err) {
            return res.send({ status: 'OK', article: article });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);
            return res.send({ error: 'Server error' });
        }
    });
});

app.put('/api/articles/:id', function (req, res) {
    return ArticleModel.findById(req.params.id, function (err, article) {
        if (!article) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }

        article.title = req.body.title;
        article.description = req.body.description;
        article.author = req.body.author;
        article.images = req.body.images;
        return article.save(function (err) {
            if (!err) {
                log.info("article updated");
                return res.send({ status: 'OK', article: article });
            } else {
                if (err.name == 'ValidationError') {
                    res.statusCode = 400;
                    res.send({ error: 'Validation error' });
                } else {
                    res.statusCode = 500;
                    res.send({ error: 'Server error' });
                }
                log.error('Internal error(%d): %s', res.statusCode, err.message);
            }
        });
    });
});

app.delete('/api/articles/:id', function (req, res) {
    return ArticleModel.findById(req.params.id, function (err, article) {
        if (!article) {
            res.statusCode = 404;
            return res.send({ error: 'Not found' });
        }
        return article.remove(function (err) {
            if (!err) {
                log.info("article removed");
                return res.send({ status: 'OK' });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
});

app.post('/api/articles/:id/comment', function (req, res) {
    var comment = new CommentsModel({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        text: req.body.text,
        article_id: req.params.id
    });
    comment.save(function (err) {
        if (!err) {
            log.info("article created");
            return res.send({ status: 'OK', article: comment });
        } else {
            if (err.name == 'ValidationError') {
                res.statusCode = 400;
                res.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                res.send({ error: 'Server error' });
            }
            log.error('Internal error(%d): %s', res.statusCode, err.message);
        }
    });
});

app.get('/api/articles/:id/comment', function (req, res) {
    CommentsModel.find({ 'article_id': req.params.id }
    ).exec(
        function (err, comments) {
            if (!comments) {
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            }
            if (!err) {
                return res.send({ status: 'OK', comment: comments });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        }
    );
});

app.put('/api/comment/:id', function (req, res) {
    CommentsModel.findByIdAndUpdate(req.params.id, {'first_name' : req.body.first_name,'last_name' : req.body.last_name, 'text' : req.body.text}, function (err) {
            if (!err) {
                return res.send({ status: 'OK'});
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        }
    );
});

app.get('/api/comment/:id', function (req, res) {
    CommentsModel.findById(req.params.id, function (err,comments) {
            if (!err) {
                return res.send({ status: 'OK', comment:comments});
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        }
    );
});

app.delete('/api/comment/:id', function (req, res) {
    CommentsModel.findByIdAndRemove(req.params.id, function (err) {
            if (!err) {
                return res.send({ status: 'OK'});
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        }
    );
});

//comments in article
app.post('/api/articles/:id/comment', function (req, res) {
    ArticleModel.update({ _id: req.params.id }, { $addToSet: { comments: {'first_name': "Andrey 333", 'last_name': "Kulik  333", 'text': "lalalla 333" } }}, function (err) {
        if (err) {
            res.statusCode = 404;
            return res.send({ error: err });
        } else {
            log.info("comment inserted");
            return res.send({ status: 'OK'});
        }

    });
});

app.get('/api/articles/:id/comment/:comment_id', function (req, res) {
    ArticleModel.find({ 'comments._id': req.params.comment_id }, "comments"
    ).exec(
        function (err, article) {
            var comment;
            article.forEach(function (val) {
                val.comments.forEach(function (vall) {
                    if (vall._id == req.params.comment_id) {
                        comment = vall;
                    }
                });
            });
            return res.send({ status: 'OK', comment: article });
            if (!comment) {
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            }
            if (!err) {
                return res.send({ status: 'OK', comment: comment });
            } else {
                res.statusCode = 500;
                log.error('Internal error(%d): %s', res.statusCode, err.message);
                return res.send({ error: 'Server error' });
            }
        }
    );
});

module.exports.app = app;