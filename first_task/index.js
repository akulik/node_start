/**
 * Created by drew on 09.02.15.
 */
var server = require("./app/server");
var router = require("./app/router");
var requestHandlers = require("./app/requestHandlers");

var handle = {}
handle["/"] = requestHandlers.start;
handle["/start"] = requestHandlers.start;
handle["/picture"] = requestHandlers.picture;
handle["/users"] = requestHandlers.users;

server.start(router.route, handle);