/**
 * Created by drew on 09.02.15.
 */
var http = require("http");
var url = require("url");

function start(route, handle) {
    function onRequest(request, response) {
        var postData = "";
        var pathname = url.parse(request.url).pathname;
        var method = request.method.toUpperCase();

        request.setEncoding("utf8");

        request.addListener("data", function(postDataChunk) {
            postData += postDataChunk;
        });

        request.addListener("end", function() {
            route(handle, pathname, response, postData, method);
        });
    }
    http.createServer(onRequest).listen(1414);
}

exports.start = start;
