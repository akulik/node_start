function route(handle, pathname, response, postData, method) {
    var mime = require('mime');
    var type = mime.lookup('pathname');

    var is_in = /\/[1-9].*/ig.test(pathname);
    var id = 0;
    if (is_in){
        var result =  /\/[1-9].*/ig.exec(pathname);
        pathname = pathname.replace(result[0],'');
        id = result[0].replace('/','');
    }

    if (typeof handle[pathname] === 'function') {
        handle[pathname](response, postData, method,id);
    } else if (type) {
        var fs = require("fs");
        fs.exists('.' + pathname, function (exists) {
            if (exists) {
                fs.readFile('.' + pathname, 'utf8', function (error, file) {
                    if (!error) {
                        fs.createReadStream('.' + pathname).pipe(response);
                    }
                })
            } else {
                fs.createReadStream("./app/view/404.html").pipe(response);
            }
        });
    } else {
        fs.createReadStream("./app/view/404.html").pipe(response);
    }
}

exports.route = route;