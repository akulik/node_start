'use strict';

/* Services */

var articlesServices = angular.module('articlesServices', ['ngResource']);

articlesServices.factory('ItemsModel', function($resource,ENDPOINT_URI) {
  return $resource(ENDPOINT_URI+'articles/:id',{id: '@id'},{ update: { method: 'PUT' }});
});


articlesServices.service('AdditionArticleInfo', function($http, ENDPOINT_URI) {
        var service = this;       

        service.articleOwner = function(articleId) {
            return $http.get(ENDPOINT_URI+'articles/'+articleId+'/owner')
        };
    });

articlesServices.service('LoginService', function($http, ENDPOINT_URI) {
        var service = this,
            path = 'Users/';

        function getUrl() {
            return ENDPOINT_URI + path;
        }

        function getLogUrl(action) {
            return getUrl() + action;
        }

        service.login = function(credentials) {
            return $http.post(getLogUrl('login'), credentials);
        };

        service.logout = function() {
            return $http.post(getLogUrl('logout'));
        };

        service.register = function(user) {
            return $http.post(getUrl(), user);
        };
    });

articlesServices.service('UserService', function(store) {
        var service = this,
            currentUser = null;
        service.setCurrentUser = function(user) {
            currentUser = user;
            store.set('user', user);
            return currentUser;
        };

        service.getCurrentUser = function() {
            if (!currentUser) {
                currentUser = store.get('user');
            }
            return currentUser;
        };

    });

articlesServices.service('APIInterceptor', function($rootScope, UserService) {
        var service = this;
        service.request = function(config) {
            var currentUser = UserService.getCurrentUser(),
                access_token = currentUser ? currentUser.access_token : null;

            if (access_token) {
                config.headers.authorization = access_token;
            }
            return config;
        };

        service.responseError = function(response) {
            if (response.status === 401) {
                $rootScope.$broadcast('unauthorized');
            }
            return response;
        };
    })